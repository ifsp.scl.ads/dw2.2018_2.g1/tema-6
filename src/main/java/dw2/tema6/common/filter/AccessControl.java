package dw2.tema6.common.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dw2.tema6.auth.model.Account;
import dw2.tema6.auth.model.AccountRepository;

@WebFilter(servletNames = {"Faces Servlet"})
public class AccessControl implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (isAllowedToPass((HttpServletRequest) request)) {
			chain.doFilter(request, response);
		} else {
			redirectToLogin((HttpServletResponse) response);
		}
	}
	
	private boolean isAllowedToPass(HttpServletRequest request) {
		if (isGoingToAdminHome(request) && !isAdminUser(request)) return false;
		return true;
	}
	
	private static Boolean isGoingToAdminHome(HttpServletRequest request) {
		return request.getRequestURI().endsWith("admin_home.xhtml");
	}
	
	private static Boolean isAdminUser(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String login = (String) session.getAttribute("user");
		Account account = AccountRepository.getAccount(login);
		return account.isAdmin();
	}

	private static void redirectToLogin(HttpServletResponse response) throws IOException {
		response.sendRedirect("login.xhtml");
	}

	@Override
	public void destroy() {
		
	}
}
