package dw2.tema6.common.controller;

import java.util.Arrays;
import java.util.Collections;

import javax.faces.bean.ManagedBean;

import dw2.tema6.auth.EncryptionHelper;
import dw2.tema6.auth.model.Account;
import dw2.tema6.auth.model.AccountRepository;
import dw2.tema6.common.InteractionManager;
import dw2.tema6.common.PersistenceManager;
import dw2.tema6.common.model.User;
import dw2.tema6.common.utils.Strings;
import dw2.tema6.project.model.Coordinator;
import dw2.tema6.system.model.Analyst;

@ManagedBean
public class AdminController {
	
	public enum UserType {
		COMMON,
		COORDINATOR,
		ANALYST,
		ADMIN;
	}

	private String name;
	private String login;
	private String password;
	private String passwordConfirmation;
	private UserType userType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}
	
	public UserType[] getUserTypes() {
		return UserType.values();
	}

	public String save() throws Exception {
		if (inputContainsError()) {
			InteractionManager.showError("fill_fields_correctly");
			return "/admin_home";
		}
		
		if (AccountRepository.getAccount(login) != null) {
			InteractionManager.showError("login_already_in_use");
			return "/admin_home";
		}
		
		User user;

		switch (userType) {
		case COORDINATOR:
			user = new Coordinator();
			break;
			
		case ANALYST:
			user = new Analyst();
			break;

		default:
			user = new User();
			break;
		}
		
		user.setName(name);
		
		Account account = new Account();
		account.setLogin(login);
		account.setPasswordHash(EncryptionHelper.generateSecureHash(password));
		account.setAdmin(userType == UserType.ADMIN);
		account.setUser(user);
		
		PersistenceManager.persist(Arrays.asList(user, account));
		
		InteractionManager.showInfo("user_added");
		return "/admin_home";
	}
	
	private Boolean inputContainsError() {
		return Strings.isEmptyOrNull(name) ||
				Strings.isEmptyOrNull(login) ||
				Strings.isEmptyOrNull(password) ||
				!password.equals(passwordConfirmation);
	}
}