package dw2.tema6.common.controller.servicerequest;

import javax.faces.bean.ManagedBean;

import dw2.tema6.auth.controller.AuthController;
import dw2.tema6.common.InteractionManager;
import dw2.tema6.common.PersistenceManager;
import dw2.tema6.common.model.ServiceRequest;
import dw2.tema6.common.model.User;
import dw2.tema6.common.utils.Strings;

@ManagedBean
public class ServiceRequestCreationController {
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String save() {
		if (inputContainsError()) {
			InteractionManager.showError("fill_fields_correctly");
			return "";
		}
		
		User user = AuthController.getLoggedUser();
		ServiceRequest request = new ServiceRequest();
		request.setDescription(description);
		request.setRequester(user);
		
		PersistenceManager.persist(request);

		InteractionManager.showInfo("request_sent");
		return "";
	}
	
	private Boolean inputContainsError() {
		return Strings.isEmptyOrNull(description);
	}
}