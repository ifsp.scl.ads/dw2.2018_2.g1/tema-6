package dw2.tema6.common.controller.servicerequest;

import java.util.List;

import javax.faces.bean.ManagedBean;

import dw2.tema6.auth.controller.AuthController;
import dw2.tema6.common.model.ServiceRequest;
import dw2.tema6.common.model.ServiceRequestRepository;
import dw2.tema6.common.model.User;

@ManagedBean
public class ServiceRequestListController {

	public List<ServiceRequest> getMyRequests() {
		User user = AuthController.getLoggedUser();
		return user.getRequests();
	}

	public List<ServiceRequest> getPendingRequests() {
		return ServiceRequestRepository.getAllPendingRequests();
	}
}