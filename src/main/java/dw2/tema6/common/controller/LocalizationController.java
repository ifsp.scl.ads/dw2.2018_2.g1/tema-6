package dw2.tema6.common.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import dw2.tema6.common.InteractionManager;

@ManagedBean
@SessionScoped
public class LocalizationController {
	private String currentLanguage;

	public String getCurrentLanguage() {
		if (currentLanguage != null)
			return currentLanguage;
		if (InteractionManager.getCurrentSession().getAttribute("language") == null) {
			InteractionManager.getCurrentSession().setAttribute("language", getViewRoot().getLocale().getLanguage());
		}
		return currentLanguage = (String) InteractionManager.getCurrentSession().getAttribute("language");
	}

	public void setCurrentLanguage(String currentLanguage) {
		this.currentLanguage = currentLanguage;
		getViewRoot().setLocale(new Locale(currentLanguage));
		InteractionManager.getCurrentSession().setAttribute("language", currentLanguage);
	}

	public List<LocalizedLocale> getSupportedLocalizedLocales() {
		List<LocalizedLocale> supportedLocales = new ArrayList<>();
		FacesContext.getCurrentInstance().getApplication().getSupportedLocales()
				.forEachRemaining(locale -> supportedLocales.add(new LocalizedLocale(locale, getCurrentLocale())));
		return supportedLocales;
	}

	public Locale getCurrentLocale() {
		return new Locale(getCurrentLanguage());
	}

	private static UIViewRoot getViewRoot() {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getViewRoot();
	}

	public class LocalizedLocale {
		private Locale locale;
		private Locale localization;

		public LocalizedLocale(Locale locale, Locale localization) {
			this.locale = locale;
			this.localization = localization;
		}

		public Locale getLocale() {
			return locale;
		}

		public String getLocalizedDisplayLanguage() {
			return locale.getDisplayLanguage(localization);
		}
	}
}
