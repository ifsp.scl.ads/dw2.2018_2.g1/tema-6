package dw2.tema6.common.utils;

public abstract class Strings {
	public static Boolean isEmptyOrNull(String string) {
		return string == null || string.isEmpty();
	}
}
