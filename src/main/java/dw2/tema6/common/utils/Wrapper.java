package dw2.tema6.common.utils;

public final class Wrapper<T> {
	public T value;
	
	public Wrapper(T initialValue) {
		value = initialValue;
	}
}