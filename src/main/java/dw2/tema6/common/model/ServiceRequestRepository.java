package dw2.tema6.common.model;

import java.util.ArrayList;
import java.util.List;

import dw2.tema6.common.PersistenceManager;

public abstract class ServiceRequestRepository {
	
	public static List<ServiceRequest> getAllPendingRequests() {
		final List<ServiceRequest> requests = new ArrayList<>();
		PersistenceManager.performQuery("select request from ServiceRequest request where request.project is null",
				query -> requests.addAll(query.getResultList()));
		return requests;
	}
}
