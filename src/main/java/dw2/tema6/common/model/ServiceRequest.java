package dw2.tema6.common.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import dw2.tema6.project.model.Project;

@Entity
public class ServiceRequest {
	
	public enum Status {
		AWAITING_ADMISSION,
		IN_PROGRESS,
		DONE
	}
	
	@Id
	@GeneratedValue
	private long number;
	private Date date = new Date();
	private String description;
	
	@ManyToOne
	private User requester;
	
	@OneToOne
	private Project project;
	
	public long getNumber() {
		return number;
	}
	
	public void setNumber(long number) {
		this.number = number;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public User getRequester() {
		return requester;
	}

	public void setRequester(User requester) {
		this.requester = requester;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	
	/**
	 * @return the completion percentage based on {@link #project} progress ranging from 0 to 1
	 * @see Project
	 */
	@Transient
	public Float getProgress() {
		if (project == null) return 0f;
		return project.getProgress();
	}
	
	/**
	 * @return the current status based on {@link #project}
	 * @see Project
	 * @see Status
	 */
	@Transient
	public Status getStatus() {
		if (project == null) return Status.AWAITING_ADMISSION;
		if (getProgress() < 1) return Status.IN_PROGRESS;
		return Status.DONE;
	}
}
