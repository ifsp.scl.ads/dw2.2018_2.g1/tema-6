package dw2.tema6.common.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Inheritance
public class User {
	
	@Id
	@GeneratedValue
	private long code;
	private String name;
	
	@OneToMany(mappedBy="requester", fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	private List<ServiceRequest> requests;
	
	public long getCode() {
		return code;
	}
	
	public void setCode(long code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public List<ServiceRequest> getRequests() {
		return requests;
	}

	public void setRequests(List<ServiceRequest> requests) {
		this.requests = requests;
	}
	
	/**
	 * @return the requested services completion percentage based on {@link #requests} progress average ranging from 0 to 1
	 * @see ServiceRequest
	 */
	@Transient
	public Float getRequestsProgress() {
		if (requests.size() == 0) return 0f;
		Float progressSum = 0f;
		for (ServiceRequest request : requests) {
			progressSum += request.getProgress();
		}
		return progressSum / requests.size();
	}
}
