package dw2.tema6.common;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import dw2.tema6.common.utils.Strings;

public abstract class InteractionManager {
	
	/**
	 * This method should be called only by ManagedBeans
	 * @return the current session get from FacesContext
	 */
	public static HttpSession getCurrentSession() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		return (HttpSession) externalContext.getSession(false);
	}

	public static void showInfo(String detail) {
		showInfo(null, detail);
	}

	public static void showInfo(String summary, String detail) {
		showMessage(summary, detail, FacesMessage.SEVERITY_INFO);
	}
	
	public static void showError(String detail) {
		showError(null, detail);
	}
	
	public static void showError(String summary, String detail) {
		showMessage(summary, detail, FacesMessage.SEVERITY_ERROR);
	}
	
	private static void showMessage(String summary, String detail, Severity severity) {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
		    ResourceBundle bundle = ResourceBundle.getBundle("strings", context.getViewRoot().getLocale());
		    if (!Strings.isEmptyOrNull(summary)) {
			    summary = bundle.getString(summary);
			}
		    if (!Strings.isEmptyOrNull(detail)) {
				detail = bundle.getString(detail);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		FacesMessage message = new FacesMessage(severity, detail, summary);
		context.addMessage(null, message);
	}
}
