package dw2.tema6.common;

import java.util.List;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import dw2.tema6.common.utils.Wrapper;

public abstract class PersistenceManager {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("tema6pu");
	
	private static void consumeManager(Consumer<EntityManager> consumer) {
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		
		consumer.accept(manager);
		
		if (manager.isOpen()) {
			manager.getTransaction().commit();
			manager.close();
		}
	}
	
	public static <T> T find(Class<T> entityClass, Object primaryKey) {
		final Wrapper<T> managedObject = new Wrapper<>(null);
		consumeManager((manager) -> managedObject.value = manager.find(entityClass, primaryKey));
		return managedObject.value;
	}
	
	public static void persist(Object managedObject) {
		consumeManager((manager) -> manager.persist(managedObject));
	}
	
	public static void persist(List<Object> managedObjects) {
		consumeManager((manager) -> {
			for (Object managedObject : managedObjects) {
				manager.persist(managedObject);
			}
		});
	}
	
	public static void update(Object managedObject) {
		consumeManager((manager) -> manager.merge(managedObject));
	}
	
	public static void update(List<Object> managedObjects) {
		consumeManager((manager) -> {
			for (Object managedObject : managedObjects) {
				manager.merge(managedObject);
			}
		});
	}
	
	public static void refresh(Object managedObject) {
		consumeManager((manager) -> manager.refresh(managedObject));
	}
	
	public static void refresh(List<Object> managedObjects) {
		consumeManager((manager) -> {
			for (Object managedObject : managedObjects) {
				manager.refresh(managedObject);
			}
		});
	}
	
	public static void remove(Object managedObject) {
		consumeManager((manager) -> manager.remove(managedObject));
	}
	
	public static void remove(List<Object> managedObjects) {
		consumeManager((manager) -> {
			for (Object managedObject : managedObjects) {
				manager.remove(managedObject);
			}
		});
	}
	
	public static void performQuery(String qlString, Consumer<Query> consumer) {
		consumeManager((manager) -> {
			Query query = manager.createQuery(qlString);
			consumer.accept(query);
		});
	}
}
