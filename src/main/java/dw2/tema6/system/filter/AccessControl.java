package dw2.tema6.system.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dw2.tema6.auth.controller.AuthController;
import dw2.tema6.auth.model.Account;
import dw2.tema6.auth.model.AccountRepository;
import dw2.tema6.common.PersistenceManager;
import dw2.tema6.common.model.User;
import dw2.tema6.common.utils.Strings;
import dw2.tema6.project.model.Coordinator;
import dw2.tema6.project.model.Project;
import dw2.tema6.system.model.Activity;
import dw2.tema6.system.model.Analyst;
import dw2.tema6.system.model.System;

@WebFilter(servletNames = {"Faces Servlet"})
public class AccessControl implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (isAllowedToPass((HttpServletRequest) request)) {
			chain.doFilter(request, response);
		} else {
			redirectToLogin((HttpServletResponse) response);
		}
	}
	
	private boolean isAllowedToPass(HttpServletRequest request) {
		if (isGoingToAnalystHome(request) && !isAnalystUser(request)) return false;
		
		if (isGoingToSystemDetails(request) && !isCoordinatorUser(request)) return false;
		if (isGoingToSystemDetails(request) && !isSystemOwner(request)) return false;
		
		if (isGoingToActivityDetails(request) && !isCoordinatorUser(request)) return false;
		if (isGoingToActivityDetails(request) && !isActivityOwner(request)) return false;
		
		return true;
	}
	
	private static Boolean isAnalystUser(HttpServletRequest request) {
		return AuthController.getLoggedUser(request.getSession()) instanceof Analyst;
	}
	
	private static Boolean isCoordinatorUser(HttpServletRequest request) {
		return AuthController.getLoggedUser(request.getSession()) instanceof Coordinator;
	}
	
	private static Boolean isGoingToAnalystHome(HttpServletRequest request) {
		return request.getRequestURI().endsWith("analyst_home.xhtml");
	}
	
	private static Boolean isGoingToSystemDetails(HttpServletRequest request) {
		return request.getRequestURI().endsWith("system_details.xhtml");
	}
	
	private static Boolean isSystemOwner(HttpServletRequest request) {
		if (request.getMethod().compareToIgnoreCase("GET") != 0) return true;
		
		String acronym = request.getParameter("id");
		if (Strings.isEmptyOrNull(acronym)) return false;
		
		System system = PersistenceManager.find(System.class, acronym);
		if (system == null) return false;
		
		User user = AuthController.getLoggedUser(request.getSession());
		if (system.getProject().getCoordinator().getCode() == user.getCode()) return true;
		return false;
	}
	
	private static Boolean isGoingToActivityDetails(HttpServletRequest request) {
		return request.getRequestURI().endsWith("activity_details.xhtml");
	}
	
	private static Boolean isActivityOwner(HttpServletRequest request) {
		if (request.getMethod().compareToIgnoreCase("GET") != 0) return true;
		
		String code = request.getParameter("id");
		if (Strings.isEmptyOrNull(code)) return false;
		Activity activity = PersistenceManager.find(Activity.class, Long.parseLong(code));
		if (activity == null) return false;
		User user = AuthController.getLoggedUser(request.getSession());
		if (activity.getSystem().getProject().getCoordinator().getCode() == user.getCode()) return true;
		return false;
	}

	private static void redirectToLogin(HttpServletResponse response) throws IOException {
		response.sendRedirect("login.xhtml");
	}

	@Override
	public void destroy() {
		
	}
}
