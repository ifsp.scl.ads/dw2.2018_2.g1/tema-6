package dw2.tema6.system.model;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Activity {
	
	public enum Status {
		TO_DO,
		DOING,
		TO_APPROVE,
		REJECTED,
		DONE;
		
		public Boolean getCanBegin() {
			return this == Status.TO_DO || this == Status.REJECTED;
		}

		public Boolean getCanFinishOrCancel() {
			return this == Status.DOING;
		}

		public Boolean getCanApproveOrDisapprove() {
			return this == Status.TO_APPROVE;
		}
	}
	
	@Id
	@GeneratedValue
	private long code;
	private String description;
	private String estimatedDuration;
	
	@Enumerated
	private Status status = Status.TO_DO;
	
	@ManyToOne
	private Analyst analyst;
	
	@ManyToOne
	private System system;

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEstimatedDuration() {
		return estimatedDuration;
	}

	public void setEstimatedDuration(String estimatedDuration) {
		this.estimatedDuration = estimatedDuration;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Analyst getAnalyst() {
		return analyst;
	}

	public void setAnalyst(Analyst analyst) {
		this.analyst = analyst;
	}

	public System getSystem() {
		return system;
	}

	public void setSystem(System system) {
		this.system = system;
	}
	
	/**
	 * @return the completion percentage based on current {@link #status} ranging from 0 to 1
	 * @see Status
	 */
	@Transient
	public Float getProgress() {
		switch (status) {
		case DOING:
			return 1f / 3;
			
		case TO_APPROVE:
			return 2f / 3;
			
		case DONE:
			return 1f;

		default:
			return 0f;
		}
	}
}
