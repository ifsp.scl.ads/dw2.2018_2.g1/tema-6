package dw2.tema6.system.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import dw2.tema6.common.model.User;

@Entity
public class Analyst extends User {
	
	@OneToMany(mappedBy="analyst", fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	private List<Activity> activities;

	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}
	
	/**
	 * @return the tasks completion percentage based on {@link #activities} progress average ranging from 0 to 1
	 * @see Activity
	 */
	@Transient
	public Float getActivitiesProgress() {
		if (activities.size() == 0) return 0f;
		Float progressSum = 0f;
		for (Activity activity : activities) {
			progressSum += activity.getProgress();
		}
		return progressSum / activities.size();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) return true;
		if (obj.getClass() != this.getClass()) return false;
		if (getCode() != ((Analyst) obj).getCode()) return false;
		return true;
	}
}
