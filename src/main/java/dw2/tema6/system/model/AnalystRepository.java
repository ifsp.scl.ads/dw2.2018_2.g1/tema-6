package dw2.tema6.system.model;

import java.util.ArrayList;
import java.util.List;

import dw2.tema6.common.PersistenceManager;

public abstract class AnalystRepository {
	
	public static Analyst getAnalyst(Long code) {
		return PersistenceManager.find(Analyst.class, code);
	}
	
	public static List<Analyst> getAllAnalysts() {
		final List<Analyst> analysts = new ArrayList<>();
		PersistenceManager.performQuery("select analyst from Analyst analyst",
				query -> analysts.addAll(query.getResultList()));
		return analysts;
	}
}
