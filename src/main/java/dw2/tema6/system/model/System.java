package dw2.tema6.system.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import dw2.tema6.project.model.Project;

@Entity
public class System {
	
	@Id
	private String acronym;
	private String name;
	
	@ManyToOne
	private Project project;
	
	@OneToMany(mappedBy="system", fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	private List<Activity> activities;

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}
	
	/**
	 * @return the completion percentage based on {@link #activities} progress average ranging from 0 to 1
	 * @see Activity
	 */
	@Transient
	public Float getProgress() {
		if (activities.size() == 0) return 0f;
		Float progressSum = 0f;
		for (Activity activity : activities) {
			progressSum += activity.getProgress();
		}
		return progressSum / activities.size();
	}
}
