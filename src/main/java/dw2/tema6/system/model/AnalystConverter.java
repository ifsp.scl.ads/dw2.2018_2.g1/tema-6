package dw2.tema6.system.model;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass=Analyst.class, value="analystConverter")
public class AnalystConverter implements Converter, Serializable {
	private static final long serialVersionUID = 1L;

	public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
	    if (modelValue == null) {
	        return "";
	    }

	    if (modelValue instanceof Analyst) {
	        return String.valueOf(((Analyst) modelValue).getCode());
	    } else {
	        throw new ConverterException(new FacesMessage(modelValue + " is not a valid Analyst"));
	    }
	}
	
	public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
	    if (submittedValue == null || submittedValue.isEmpty()) {
	        return null;
	    }

	    try {
	        return AnalystRepository.getAnalyst(Long.valueOf(submittedValue));
	    } catch (NumberFormatException e) {
	        throw new ConverterException(new FacesMessage(submittedValue + " is not a valid Analyst ID"), e);
	    }
	}
}
