package dw2.tema6.system.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;

import dw2.tema6.common.PersistenceManager;
import dw2.tema6.system.model.Activity;
import dw2.tema6.system.model.System;

@ManagedBean
@ViewScoped
public class SystemDetailsController {
	private String systemAcronym;
	private System system;
	private String name;
	
	public String getSystemAcronym() {
		return systemAcronym;
	}

	public void setSystemAcronym(String systemAcronym) {
		this.systemAcronym = systemAcronym;
		system = PersistenceManager.find(System.class, systemAcronym);
		name = system.getName();
	}

	public System getSystem() {
		return system;
	}

	public void setSystem(System system) {
		this.system = system;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		system.setName(name);
	}

	public void didChangeValue(AjaxBehaviorEvent event) {
		PersistenceManager.update(system);
	}

	public String addActivity() {
		Activity activity = new Activity();
		activity.setSystem(system);
		PersistenceManager.persist(activity);
		
		system.getActivities().add(activity);
		PersistenceManager.update(system);
		
		return "activity_details?faces-redirect=true&id=" + activity.getCode();
	}
}