package dw2.tema6.system.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import dw2.tema6.common.PersistenceManager;
import dw2.tema6.system.model.Activity;

@ManagedBean
@ViewScoped
public class ActivityWorkflowController {
	private Activity activity;

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	
	public String begin() {
		activity.setStatus(Activity.Status.DOING);
		PersistenceManager.update(activity);
		return "";
	}

	public String finish() {
		activity.setStatus(Activity.Status.TO_APPROVE);
		PersistenceManager.update(activity);
		return "";
	}

	public String cancel() {
		activity.setStatus(Activity.Status.TO_DO);
		PersistenceManager.update(activity);
		return "";
	}
}
