package dw2.tema6.system.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import dw2.tema6.auth.controller.AuthController;
import dw2.tema6.common.PersistenceManager;
import dw2.tema6.project.model.Coordinator;
import dw2.tema6.project.model.Project;
import dw2.tema6.system.model.Activity;
import dw2.tema6.system.model.Analyst;

@ManagedBean
@ViewScoped
public class ActivityListController {
	public List<Activity> getMyActivities() {
		Analyst analyst = (Analyst) AuthController.getLoggedUser();
		return analyst.getActivities();
	}
}