package dw2.tema6.system.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;

import dw2.tema6.common.InteractionManager;
import dw2.tema6.common.PersistenceManager;
import dw2.tema6.system.model.Activity;
import dw2.tema6.system.model.Analyst;
import dw2.tema6.system.model.AnalystRepository;

@ManagedBean
@ViewScoped
public class ActivityDetailsController {
	private Long activityCode;
	private Activity activity;
	private String description;
	private String estimatedDuration;
	private Analyst analyst;
	private Analyst previousAnalyst;

	public Long getActivityCode() {
		return activityCode;
	}

	public void setActivityCode(Long activityCode) {
		this.activityCode = activityCode;
		activity = PersistenceManager.find(Activity.class, activityCode);
		description = activity.getDescription();
		estimatedDuration = activity.getEstimatedDuration();
		analyst = activity.getAnalyst();
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		activity.setDescription(description);
	}

	public String getEstimatedDuration() {
		return estimatedDuration;
	}

	public void setEstimatedDuration(String estimatedDuration) {
		this.estimatedDuration = estimatedDuration;
		activity.setEstimatedDuration(estimatedDuration);
	}

	public Analyst getAnalyst() {
		return analyst;
	}

	public void setAnalyst(Analyst analyst) {
		this.analyst = analyst;
		previousAnalyst = activity.getAnalyst();
		activity.setAnalyst(analyst);
		
		if (previousAnalyst != null) {
			previousAnalyst.getActivities().remove(activity);
		}
		
		if (analyst != null) {
			analyst.getActivities().add(activity);
		}
	}

	public List<Analyst> getAllAnalysts() {
		return AnalystRepository.getAllAnalysts();
	}
	
	public void didChangeValue(AjaxBehaviorEvent event) {
		save();
	}

	public void save() {
		PersistenceManager.update(activity);
		
		if (previousAnalyst != null) {
			PersistenceManager.update(previousAnalyst);
		}
		
		if (analyst != null) {
			PersistenceManager.update(analyst);
		}
	}

	public String approve() {
		activity.setStatus(Activity.Status.DONE);
		save();
		return "";
	}

	public String disapprove() {
		activity.setStatus(Activity.Status.REJECTED);
		save();
		return "";
	}
}