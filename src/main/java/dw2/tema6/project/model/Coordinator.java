package dw2.tema6.project.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import dw2.tema6.common.model.User;

@Entity
public class Coordinator extends User {
	
	@OneToMany(mappedBy="coordinator", fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	private List<Project> projects;

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	
	/**
	 * @return the running projects completion percentage based on {@link #projects} progress average ranging from 0 to 1
	 * @see Project
	 */
	@Transient
	public Float getProjectsProgress() {
		if (projects.size() == 0) return 0f;
		Float progressSum = 0f;
		for (Project project : projects) {
			progressSum += project.getProgress();
		}
		return progressSum / projects.size();
	}
}
