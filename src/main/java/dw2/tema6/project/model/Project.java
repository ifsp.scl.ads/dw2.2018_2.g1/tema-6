package dw2.tema6.project.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import dw2.tema6.common.model.ServiceRequest;
import dw2.tema6.system.model.System;

@Entity
public class Project {
	
	@Id
	@GeneratedValue
	private long number;
	private String description;
	
	@OneToOne(mappedBy="project")
	private ServiceRequest request;
	
	@ManyToOne
	private Coordinator coordinator;
	
	@OneToMany(mappedBy="project", fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	private List<System> systems;

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ServiceRequest getRequest() {
		return request;
	}

	public void setRequest(ServiceRequest request) {
		this.request = request;
	}

	public Coordinator getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(Coordinator coordinator) {
		this.coordinator = coordinator;
	}

	public List<System> getSystems() {
		return systems;
	}

	public void setSystems(List<System> systems) {
		this.systems = systems;
	}
	
	/**
	 * @return the completion percentage based on {@link #systems} progress average ranging from 0 to 1
	 * @see System
	 */
	@Transient
	public Float getProgress() {
		if (systems.size() == 0) return 0f;
		Float progressSum = 0f;
		for (System system : systems) {
			progressSum += system.getProgress();
		}
		return progressSum / systems.size();
	}
}
