package dw2.tema6.project.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dw2.tema6.auth.controller.AuthController;
import dw2.tema6.auth.model.Account;
import dw2.tema6.auth.model.AccountRepository;
import dw2.tema6.common.PersistenceManager;
import dw2.tema6.common.model.User;
import dw2.tema6.common.utils.Strings;
import dw2.tema6.project.model.Coordinator;
import dw2.tema6.project.model.Project;
import dw2.tema6.system.model.Analyst;

@WebFilter(servletNames = {"Faces Servlet"})
public class AccessControl implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (isAllowedToPass((HttpServletRequest) request)) {
			chain.doFilter(request, response);
		} else {
			redirectToLogin((HttpServletResponse) response);
		}
	}
	
	private boolean isAllowedToPass(HttpServletRequest request) {
		if (isGoingToCoordinatorHome(request) && !isCoordinatorUser(request)) return false;
		
		if (isGoingToProjectDetails(request) && !isCoordinatorUser(request)) return false;
		if (isGoingToProjectDetails(request) && !isProjectOwner(request)) return false;
		
		return true;
	}
	
	private static Boolean isCoordinatorUser(HttpServletRequest request) {
		return AuthController.getLoggedUser(request.getSession()) instanceof Coordinator;
	}
	
	private static Boolean isGoingToCoordinatorHome(HttpServletRequest request) {
		return request.getRequestURI().endsWith("coordinator_home.xhtml");
	}
	
	private static Boolean isGoingToProjectDetails(HttpServletRequest request) {
		return request.getRequestURI().endsWith("project_details.xhtml");
	}
	
	private static Boolean isProjectOwner(HttpServletRequest request) {
		if (request.getMethod().compareToIgnoreCase("GET") != 0) return true;
		
		String number = request.getParameter("id");
		if (Strings.isEmptyOrNull(number)) return false;
		
		Project project = PersistenceManager.find(Project.class, Long.parseLong(number));
		if (project == null) return false;
		
		User user = AuthController.getLoggedUser(request.getSession());
		if (project.getCoordinator().getCode() == user.getCode()) return true;
		return false;
	}

	private static void redirectToLogin(HttpServletResponse response) throws IOException {
		response.sendRedirect("login.xhtml");
	}

	@Override
	public void destroy() {
		
	}
}
