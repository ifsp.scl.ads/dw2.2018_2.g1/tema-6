package dw2.tema6.project.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import dw2.tema6.auth.controller.AuthController;
import dw2.tema6.common.PersistenceManager;
import dw2.tema6.common.model.ServiceRequest;
import dw2.tema6.project.model.Coordinator;
import dw2.tema6.project.model.Project;

@ManagedBean
@ViewScoped
public class ProjectCreationController {
	private ServiceRequest serviceRequest;

	public ServiceRequest getServiceRequest() {
		return serviceRequest;
	}

	public void setServiceRequest(ServiceRequest serviceRequest) {
		this.serviceRequest = serviceRequest;
	}

	public String create() {
		Coordinator coordinator = (Coordinator) AuthController.getLoggedUser();
		
		Project project = new Project();
		project.setCoordinator(coordinator);
		PersistenceManager.persist(project);
		
		serviceRequest.setProject(project);
		PersistenceManager.update(serviceRequest);
		
		return "project_details?faces-redirect=true&id=" + project.getNumber();
	}
}