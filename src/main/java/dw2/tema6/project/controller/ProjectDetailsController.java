package dw2.tema6.project.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;

import dw2.tema6.common.PersistenceManager;
import dw2.tema6.project.model.Project;
import dw2.tema6.system.model.System;

@ManagedBean
@ViewScoped
public class ProjectDetailsController {
	private Long projectNumber;
	private Project project;
	private String description;
	
	public Long getProjectNumber() {
		return projectNumber;
	}

	public void setProjectNumber(Long projectNumber) {
		this.projectNumber = projectNumber;
		project = PersistenceManager.find(Project.class, projectNumber);
		description = project.getDescription();
	}
	
	public Project getProject() {
		return project;
	}
	
	public void setProject(Project project) {
		this.project = project;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		project.setDescription(description);
	}

	public void didChangeValue(AjaxBehaviorEvent event) {
		PersistenceManager.update(project);
	}

	public String addSystem() {
		System system = new System();
		system.setProject(project);
		system.setAcronym(project.getNumber() + "-" + project.getSystems().size());
		PersistenceManager.persist(system);
		
		project.getSystems().add(system);
		PersistenceManager.update(project);
		
		return "system_details?faces-redirect=true&id=" + system.getAcronym();
	}
}