package dw2.tema6.project.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import dw2.tema6.auth.controller.AuthController;
import dw2.tema6.common.PersistenceManager;
import dw2.tema6.project.model.Coordinator;
import dw2.tema6.project.model.Project;

@ManagedBean
@ViewScoped
public class ProjectListController {
	public List<Project> getMyProjects() {
		Coordinator coordinator = (Coordinator) AuthController.getLoggedUser();
		return coordinator.getProjects();
	}
}