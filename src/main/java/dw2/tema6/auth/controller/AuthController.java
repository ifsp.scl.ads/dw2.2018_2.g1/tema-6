package dw2.tema6.auth.controller;

import javax.faces.bean.ManagedBean;
import javax.servlet.http.HttpSession;

import dw2.tema6.auth.EncryptionHelper;
import dw2.tema6.auth.model.Account;
import dw2.tema6.auth.model.AccountRepository;
import dw2.tema6.common.InteractionManager;
import dw2.tema6.common.model.User;
import dw2.tema6.common.utils.Strings;
import dw2.tema6.project.model.Coordinator;
import dw2.tema6.system.model.Analyst;

@ManagedBean
public class AuthController {
	private String login;
	private String password;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String authenticate() throws Exception {
		Account account = AccountRepository.getAccount(login);
		
		if (account == null || accountRefusesPassword(account, password)) {
			InteractionManager.showError("invalid_credentials");
			return "/login";
		}
		
		InteractionManager.getCurrentSession().setAttribute("user", login);
		
		if (account.isAdmin()) {
			return "/admin_home";
		}
		
		if (account.getUser() instanceof Coordinator) {
			return "/coordinator_home";
		} else if (account.getUser() instanceof Analyst) {
			return "/analyst_home";
		}
		
		return "/common_home";
	}
	
	private static Boolean accountRefusesPassword(Account account, String password) throws Exception {
		return !EncryptionHelper.matchesSecureHash(password, account.getPasswordHash());
	}

	public String deauthenticate() {
		InteractionManager.getCurrentSession().removeAttribute("user");
		return "/login";
	}
	
	public User getCurrentUser() {
		return getLoggedUser();
	}
	
	/**
	 * This method should be called only by ManagedBeans
	 * @return logged user based on current session
	 */
	public static User getLoggedUser() {
		return getLoggedUser(InteractionManager.getCurrentSession());
	}

	public static User getLoggedUser(HttpSession session) {
		String userLogin = (String) session.getAttribute("user");
		if (Strings.isEmptyOrNull(userLogin)) return null;
		
		return AccountRepository.getAccount(userLogin).getUser();
	}
}