package dw2.tema6.auth.controller;

import java.util.Arrays;

import javax.faces.bean.ManagedBean;

import dw2.tema6.auth.EncryptionHelper;
import dw2.tema6.auth.model.Account;
import dw2.tema6.common.InteractionManager;
import dw2.tema6.common.PersistenceManager;
import dw2.tema6.common.utils.Strings;
import dw2.tema6.common.model.User;

@ManagedBean
public class InitialSetupController {
	private String login;
	private String password;
	private String passwordConfirmation;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

	public String save() throws Exception {
		if (inputContainsError()) {
			InteractionManager.showError("invalid_credentials");
			return "/initial_setup";
		}
		
		User user = new User();
		user.setName("Admin");
		
		Account account = new Account();
		account.setLogin(login);
		account.setPasswordHash(EncryptionHelper.generateSecureHash(password));
		account.setAdmin(true);
		account.setUser(user);
		
		PersistenceManager.persist(Arrays.asList(user, account));
		InteractionManager.getCurrentSession().setAttribute("user", login);
		
		return "/admin_home";
	}
	
	private Boolean inputContainsError() {
		return Strings.isEmptyOrNull(login) || Strings.isEmptyOrNull(password) || !password.equals(passwordConfirmation);
	}
}