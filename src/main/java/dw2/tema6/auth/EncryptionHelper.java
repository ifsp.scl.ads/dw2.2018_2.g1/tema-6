package dw2.tema6.auth;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public abstract class EncryptionHelper {
	
	public static String generateSecureHash(String content) throws NoSuchAlgorithmException, InvalidKeySpecException {
		int iterations = 1000;
		byte[] salt = getRandomSalt();
		
		PBEKeySpec spec = new PBEKeySpec(content.toCharArray(), salt, iterations, 64 * 8);
		byte[] hash = keyFactory().generateSecret(spec).getEncoded();
		
		return iterations + ":" + toHex(salt) + ":" + toHex(hash);
	}
	
	public static Boolean matchesSecureHash(String content, String secureHashedContent) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String[] parts = secureHashedContent.split(":");
		int iterations = Integer.parseInt(parts[0]);
		byte[] salt = fromHex(parts[1]);
		byte[] hash = fromHex(parts[2]);
		
		PBEKeySpec spec = new PBEKeySpec(content.toCharArray(), salt, iterations, hash.length * 8);
		byte[] testHash = keyFactory().generateSecret(spec).getEncoded();
		
		int diff = hash.length ^ testHash.length;
		for (int i = 0; i < hash.length && i < testHash.length; i++) {
			diff |= hash[i] ^ testHash[i];
		}
		return diff == 0;
	}
	
	private static SecretKeyFactory keyFactory() throws NoSuchAlgorithmException {
		return SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
	}
	
	private static byte[] getRandomSalt() throws NoSuchAlgorithmException {
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
		byte[] salt = new byte[16];
		random.nextBytes(salt);
		return salt;
	}
	
	private static String toHex(byte[] bytes) throws NoSuchAlgorithmException {
		BigInteger integer = new BigInteger(1, bytes);
		String hex = integer.toString(16);
		int paddingLength = (bytes.length * 2) - hex.length();
		
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + hex;
		} else {
			return hex;
		}
	}
	
	private static byte[] fromHex(String hex) throws NoSuchAlgorithmException {
		byte[] bytes = new byte[hex.length() / 2];
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return bytes;
	}
}
