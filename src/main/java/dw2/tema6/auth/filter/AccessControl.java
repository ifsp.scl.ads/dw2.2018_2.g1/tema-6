package dw2.tema6.auth.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dw2.tema6.auth.model.AccountRepository;

@WebFilter(servletNames = {"Faces Servlet"})
public class AccessControl implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		if (isAllowedToPass((HttpServletRequest) request)) {
			chain.doFilter(request, response);
			
		} else if (isPristine()) {
			redirectToInitialSetup((HttpServletResponse) response);
			
		} else {
			redirectToLogin((HttpServletResponse) response);
		}
	}

	private static Boolean isPristine() {
		return !AccountRepository.hasRegisteredAccounts();
	}
	
	private static Boolean isGoingToInitialSetupPage(HttpServletRequest request) {
		return request.getRequestURI().endsWith("initial_setup.xhtml");
	}
	
	private void redirectToInitialSetup(HttpServletResponse response) throws IOException {
		response.sendRedirect("initial_setup.xhtml");
	}
	
	private static Boolean isAllowedToPass(HttpServletRequest request) {
		if (isPristine() ^ isGoingToInitialSetupPage(request)) return false;
		if (isPristine() && isGoingToInitialSetupPage(request)) return true;
		
		if (isAuthenticated(request) || isGoingToLoginPage(request)) return true;
		return false;
	}
	
	private static Boolean isAuthenticated(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return session.getAttribute("user") != null;
	}
	
	private static Boolean isGoingToLoginPage(HttpServletRequest request) {
		return request.getRequestURI().endsWith("login.xhtml");
	}
	
	private static void redirectToLogin(HttpServletResponse response) throws IOException {
		response.sendRedirect("login.xhtml");
	}

	@Override
	public void destroy() {
		
	}
}
