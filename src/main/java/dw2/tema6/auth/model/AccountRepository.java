package dw2.tema6.auth.model;

import dw2.tema6.common.PersistenceManager;
import dw2.tema6.common.utils.Wrapper;

public abstract class AccountRepository {

	public static Account getAccount(String login) {
		return PersistenceManager.find(Account.class, login);
	}
	
	public static Boolean hasRegisteredAccounts() {
		final Wrapper<Long> accounts = new Wrapper<>(0L);
		PersistenceManager.performQuery("select count(*) from Account", (query) -> accounts.value = (long) query.getSingleResult());
		return accounts.value != 0;
	}
}
